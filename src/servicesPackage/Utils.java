package servicesPackage;

import java.util.*;
import java.util.Map.Entry;

public class Utils {


    Comparator<Entry<String, double[]>> valueComparator = new Comparator<Entry<String, double[]>>() {

        @Override
        public int compare(Entry<String, double[]> entry1, Entry<String, double[]> entry2)
        {
            double calories1 = entry1.getValue()[0];
            double calories2 = entry2.getValue()[0];

            if (calories1 == calories2) {
                double rate1 = entry1.getValue()[1];
                double rate2 = entry2.getValue()[1];

                if (rate1 == rate2) return 0;

                return rate1 > rate2? -1 : 1;
            }

            return calories1 > calories2 ? -1 : 1;
        }
    };
}
