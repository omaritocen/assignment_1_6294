package guiPackage;

import activityPackage.Activity;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import servicesPackage.ActivityService;

import java.io.IOException;

public class MainPageController {
    public TextField timeTextField;
    public ComboBox activityComboBox;
    public Button submitButton;
    public Label totalCaloriesBurnt;
    public Label heartRateInc;
    public Button getDetailsButton;
    public Button getRankingsButton;

    private ActivityService _activityService;

    public MainPageController()
    {
        _activityService = new ActivityService();
    }

    @FXML
    private void submit()
    {
        if (!validateInput()) return;
        String timeText = timeTextField.getText();

        String activityName = activityComboBox.getValue().toString();

        Activity activity = _activityService.createNewActivity(activityName, timeText);
        ActivityService.Activities.add(activity);

        totalCaloriesBurnt.setText(String.valueOf(_activityService.getBurntCalories()));
        heartRateInc.setText(_activityService.getCurrentHeartInc() + " beats/min");

        showSuccessAlert("Information", "Activity is recorded successfully!");
    }

    @FXML
    private void getDetails() {
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("detailsWindow.fxml"));
            Stage detailsStage = new Stage();
            detailsStage.setTitle("Activities Details");
            Scene sc = new Scene(root, 800, 600);
            sc.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            detailsStage.setScene(sc);
            detailsStage.show();
        } catch (IOException ex) {
            showErrorAlert("Missing Data", "Please enter any activity before accessing details");
            System.out.println("Error" + ex);
        } 
    }

    @FXML
    private void getRanking() {
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("rankingWindow.fxml"));
            Stage detailsStage = new Stage();
            detailsStage.setTitle("Activities Ranking");
            Scene sc = new Scene(root, 800, 600);
            sc.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            detailsStage.setScene(sc);
            detailsStage.show();
        } catch (IOException ex) {
            System.out.println("Error" + ex);
        }
    }

    private void showSuccessAlert(String header, String errorMsg)
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Success!");
        alert.setHeaderText(header);
        alert.setContentText(errorMsg);
        alert.showAndWait();
    }

    private void showErrorAlert(String header, String errorMsg)
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error!");
        alert.setHeaderText(header);
        alert.setContentText(errorMsg);
        alert.showAndWait();
    }

    private boolean validateInput()
    {
        String timeText = timeTextField.getText();

        try {
            String activityName = activityComboBox.getValue().toString();
        } catch (NullPointerException ex) {
            showErrorAlert("Missing Field", "You must select an activity");
            return false;
        }

        if (timeText.equals("")) {
            showErrorAlert("Missing Field", "You must enter a time for the activity");
            return false;
        }

        int t;
        try {
            t = Integer.parseInt(timeText);
        } catch (NumberFormatException ex) {
            showErrorAlert("Unaccepted value", "You should enter a integer for the time");
            return false;
        }

        if (t <= 0) {
            showErrorAlert("Unaccepted value", "You should enter a positive number for the time");
            return false;
        }

        return true;
    }
}
