package activityPackage;

public class Running extends Activity {

    public Running(int time) {
        super(time);
        setCaloriesBurnRate(5);
        setHeartIncPercentage(.3);
    }
}
