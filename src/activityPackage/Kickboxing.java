package activityPackage;

public class Kickboxing extends Activity {
    public Kickboxing(int time)
    {
        super(time);
        setCaloriesBurnRate(3);
        setHeartIncPercentage(.5);
    }
}
