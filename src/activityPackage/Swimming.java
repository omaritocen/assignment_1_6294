package activityPackage;

public class Swimming extends Activity {
    public Swimming(int time) {
        super(time);
        setCaloriesBurnRate(4);
        setHeartIncPercentage(.2);
    }
}
